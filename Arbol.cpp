#include <iostream>
#include "Arbol.h"

using namespace std;

Arbol::Arbol(){

}

// Crea un nuevo nodo
Nodo* Arbol::crear_nodo(int numero, Nodo *padre){
    Nodo *temporal;

    temporal = new Nodo;  

    // Se le asigna un valor al nodo
    temporal->dato = numero;

    // Se crean los subarboles izquierdo y derecho
    temporal->izq = NULL;
    temporal->der = NULL;
    temporal->nodo_padre = padre;
    
    return temporal;
}

// Insertara el nodo en el subarbol correspondiente
void Arbol::insertar(Nodo *&raiz, int numero, Nodo *padre){
    int valor_raiz;
    
    if(raiz == NULL){
        Nodo *temporal;
        temporal = crear_nodo(numero, padre);
        raiz = temporal;
        insertar(raiz, numero, padre);
    }

    // Compara el valor de la raiz con el que se quiere ingresar
    else{
        valor_raiz = raiz->dato;
        if(numero < valor_raiz){
            // Si el número ingresado es menor a la raiz se ingresara al lado izquierdo
            insertar(raiz->izq, numero, raiz); 
        }

        else if(numero > valor_raiz){
            // Si el número ingresado es mayor a la raiz se ingresara al lado derecho
            insertar(raiz->der, numero, raiz);
        }
    }
}

Nodo* Arbol::encontrar_mas_izquierdo(Nodo *raiz){
    if(raiz->izq){
        // Retorna el nodo más pequeño
        return encontrar_mas_izquierdo(raiz->izq);
    }

    else{
        return raiz;
    }
}

void Arbol::destruir_nodo(Nodo *nodo){
    nodo->izq = NULL;
    nodo->der = NULL;
}

// Se reemplaza el nodo que se quiere eliminar por su hijo
void Arbol::reemplar_hijo(Nodo *raiz, Nodo *cambio){
    if(raiz->nodo_padre){
        // cambio por hijo izquierdo
        if(raiz->dato == raiz->nodo_padre->izq->dato){
            raiz->nodo_padre->izq = cambio;
        }

        // cambio por hijo derecho
        else if(raiz->dato == raiz->nodo_padre->der->dato){
            raiz->nodo_padre->der = cambio;
        }
    }
    
    // Cambia el padre del nuevo nodo por el del nodo que se elimino
    if(cambio){
        cambio->nodo_padre = raiz->nodo_padre;
    }
}

void Arbol::eliminar_nodo(Nodo *&nodo_eliminado){
    // El nodo tiene 2 hijos
    if(nodo_eliminado->izq  && nodo_eliminado->der){
        // Se buscara el nodo más izquierdo para reemplazarlo en la posición del que se eliminara
        Nodo *menor = encontrar_mas_izquierdo(nodo_eliminado->der);
        nodo_eliminado->dato = menor->dato;
        eliminar_nodo(menor);
    }
    
    // Hijo en el lado izquierdo
    else if(nodo_eliminado->izq){
        reemplar_hijo(nodo_eliminado, nodo_eliminado->izq);
        destruir_nodo(nodo_eliminado);
    }

    // Hijo en el lado derecho
    else if(nodo_eliminado->der){
        reemplar_hijo(nodo_eliminado, nodo_eliminado->der);
        destruir_nodo(nodo_eliminado);
    }

    // No tiene hijos
    else{
        reemplar_hijo(nodo_eliminado, NULL);
    }
}

// Eliminar un nodo del árbol
void Arbol::eliminar(Nodo *&raiz, int numero){
 
    // Si el número que se quiere eliminar es menor
    if(numero < raiz->dato){
        eliminar(raiz->izq, numero);
    }

    // Si el número que se quiere eliminar es mayor
    else if(numero > raiz->dato){
        eliminar(raiz->der, numero);
    }
    
    else{
        eliminar_nodo(raiz);

    }
}

void Arbol::mostrar_arbol_inorden(Nodo *raiz){
    if(raiz != NULL){
        mostrar_arbol_inorden(raiz->izq);
        cout << raiz->dato << " - ";
        mostrar_arbol_inorden(raiz->der);
    }
}

void Arbol::mostrar_arbol_preorden(Nodo *raiz){
    if(raiz != NULL){
        cout << raiz->dato << " - ";
        mostrar_arbol_preorden(raiz->izq);
        mostrar_arbol_preorden(raiz->der);
    }
}

void Arbol::mostrar_arbol_posorden(Nodo *raiz){
    if(raiz != NULL){
        mostrar_arbol_posorden(raiz->izq);
        mostrar_arbol_posorden(raiz->der);
        cout << raiz->dato << " - ";
    }
}

