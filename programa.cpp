#include <iostream>
#include <fstream>
#include <stdio.h>
#include <string.h>
#include "Arbol.h"

using namespace std;

/* para usar fork() */
#include <unistd.h>

class Graficar {
    private:
        Nodo *arbol = NULL;

    public:
        // Constructor de la Clase Graficar.
        Graficar(Nodo *raiz) {
	        this->arbol = raiz;
        }

        // ofstream es el tipo de dato correspondiente a archivos en cpp (el llamado es ofstream &nombre_archivo).
        void recorrerArbol(Nodo *p, ofstream &archivo) {
	        string infoTmp;
	        /* Se enlazan los nodos del grafo, para diferencia entre izq y der a cada nodo se le entrega un identificador*/
	        if (p != NULL) {
	        // Por cada nodo ya sea por izq o der se escribe dentro de la instancia del archivo.
	        if (p->izq != NULL) {
		        archivo<< p->dato << "->" << p->izq->dato << ";" << endl;
	        }

	        else {
		        infoTmp = to_string(p->dato) + "i";
		        infoTmp = "\"" + infoTmp + "\"";
		        archivo << infoTmp << "[shape=point]" << endl;
		        archivo << p->dato << "->" << infoTmp << ";" << endl;
	        }
	        
	        infoTmp = p->dato;
	        
	        if (p->der != NULL) {
		        archivo << p->dato << "->" << p->der->dato << ";" << endl;
	        }

	        else {
		        infoTmp = to_string(p->dato) + "d";
		        infoTmp = "\"" + infoTmp + "\"";
		        archivo << infoTmp << "[shape=point]" << endl;
		        archivo << p->dato << "->" << infoTmp << ";" << endl;
	        }
	        
	        // Se realizan los llamados tanto por la izquierda como por la derecha para la creación del grafo.
	        recorrerArbol(p->izq, archivo);
	        recorrerArbol(p->der, archivo);
	        }

	        return;
        }

        void crearGraficar() {
	        ofstream archivo;
	        // Se abre/crea el archivo datos.txt, a partir de este se generará el grafo.
	        archivo.open("datos.txt");
	        // Se escribe dentro del archivo datos.txt "digraph G { ".
	        archivo << "digraph G {" << endl;
	        // Se pueden cambiar los colores que representarán a los nodos, para el ejemplo el color será verde.
	        archivo << "node [style=filled fillcolor=cyan];" << endl;
	        // Llamado a la función recursiva que genera el archivo de texto para creación del grafo.
	        recorrerArbol(this->arbol, archivo);
	        // Se termina de escribir dentro del archivo datos.txt.
	        archivo << "}" << endl;
	        archivo.close();
	        
	        // Genera el grafo.
	        system("dot -Tpng -ografo.png datos.txt &");
	        system("eog grafo.png &");
        }
};

// Valida que el ingreso de números sea correcto
bool validar_entrada(string entrada){
    bool valido = true;
    
    for(int i = 0; i < entrada.size(); i++){
        // Valores del código ascii
        // 48 = 0 y 57 = 9
        if(entrada[i] < 48 || entrada[i] > 57){
            valido = false;
        }
    }

    return valido;
}

void menu_recorrido(Arbol *nuevo_arbol, Nodo *raiz){
    int tipo_orden;

    system("clear");

    do{
        cout << "\n\n\tTipos de recorrido" << endl;
        cout << " [1] Preorden" << endl;
        cout << " [2] Inorden" << endl;
        cout << " [3] Posorden" << endl;
        cout << " [4] Volver al menú principal" << endl;

        cout << "\n Ingrese su opción: ";
        cin >> tipo_orden;

        if(raiz != NULL){
            if(tipo_orden == 1){
                system("clear");
                cout << "\nÁRBOL EN PREORDEN" << endl;
                nuevo_arbol->mostrar_arbol_preorden(raiz);
            }
        
            else if(tipo_orden == 2){
                system("clear");
                cout << "\nÁRBOL EN INORDEN" << endl;
                nuevo_arbol->mostrar_arbol_inorden(raiz);
            }

            else if(tipo_orden == 3){
                system("clear");
                cout << "\nÁRBOL EN POSORDEN" << endl;
                nuevo_arbol->mostrar_arbol_posorden(raiz);
            }
        }
        
        else{
            cout << "\nEl árbol esta vacio" << endl;
        }

    }

    while(tipo_orden != 4);

}

void eliminar_nodo(Arbol *nuevo_arbol, Nodo *raiz){
    bool valido_eliminar;
    string numero_eliminado;

    cout << "Ingrese el número del nodo que eliminara: ";
    cin.ignore();
    getline(cin, numero_eliminado);

    // Valida que solo se ingresen números
    valido_eliminar = validar_entrada(numero_eliminado);

    if(valido_eliminar == true){
        nuevo_arbol->eliminar(raiz, stoi(numero_eliminado));
    }

    else{
        cout << "\n" << numero_eliminado << " NO CORRESPONDE A UN NÚMERO " << endl;
        sleep(3);
    }
}

void ingresar_nodo(Arbol *nuevo_arbol, Nodo *&raiz){
    bool valido_numero;
    string num;
    Nodo *nodo = NULL;

    system("clear");
    cout << "\nDigite un número: ";
    cin.ignore();
    getline(cin, num);

    valido_numero = validar_entrada(num);

    if(valido_numero == true){
        nodo = nuevo_arbol->crear_nodo(stoi(num), NULL);

        // Se inicializa la raiz del arbol
        if(raiz == NULL){
            raiz = nodo;
        }

        // Cuando hay más de un nodo           
        else{
            nuevo_arbol->insertar(raiz, stoi(num), NULL);
        }
    }

    else{
        cout << "\n" << num << " NO CORRESPONDE A UN NÚMERO " << endl;
        sleep(3);
    }
}

void menu(Arbol *nuevo_arbol, Nodo *raiz){
    int opcion; 

    do{ 

        cout << "\nMENU\n" << endl;
        cout << " [1] Ingresar número" << endl;
        cout << " [2] Mostrar árbol" << endl;
        cout << " [3] Eliminar nodo" << endl;
        cout << " [4] Modificar nodo" << endl;
        cout << " [5] Mostrar grafo" << endl;
        cout << " [6] Salir" << endl;
     
        cout << "\n Ingrese su opción: ";
        cin >> opcion;
       
        switch(opcion){
            case 1:
                // Crea el nodo y se ingresa el dato
                ingresar_nodo(nuevo_arbol, raiz);
                break;

            case 2:
                // Resultados de los diferentes recorridos
                menu_recorrido(nuevo_arbol, raiz);
                break;
        
            case 3:
                // Se elimina un nodo seleccionado
                eliminar_nodo(nuevo_arbol, raiz);
                break;

            case 4:
                // ARREGLAR!!
                eliminar_nodo(nuevo_arbol, raiz);
                ingresar_nodo(nuevo_arbol, raiz);
                break;

            case 5:
                // Se mostrara la representación grafica del árbol
                Graficar *g = new Graficar(raiz);
                g->crearGraficar();
                break;
        }

        system("clear");
    }

    while(opcion != 6);
}

int main(){
    Arbol *nuevo_arbol = new Arbol();
    Nodo *raiz = NULL;
    
    menu(nuevo_arbol, raiz);

    return 0;
}
